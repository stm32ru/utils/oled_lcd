#include <stdarg.h>
#include <stdio.h>

#include "gpio.h"
#include "i2c.h"
#include "rcc.h"

#include "SSD1306_driver.h"
#include "oled_fonts.h"
#include "stdbool.h"

static i2cInstance_t OLEDi2c;

static const uint8_t initBuffer[] = { 0x80, //control byte
		SSD1306_DISPLAYOFF, 0x00, //Low Column
		SSD1306_SETHIGHCOLUMN, 0xB0, //Page
		SSD1306_SETSTARTLINE, 0xA1, //remap
		SSD1306_SETCOMPINS, 0x12,
		SSD1306_SETDISPLAYOFFSET, 0x00, //NO offset
		SSD1306_COMSCANINC,
		SSD1306_COMSCANDEC,
		SSD1306_NORMALDISPLAY,
		SSD1306_DISPLAYALLON_RESUME, //display ON
		SSD1306_SETCONTRAST, //set contrast
		0x50, //contrast DATA
		SSD1306_SETMULTIPLEX, //multiplex ratio
		0x3f, //1/64 duty
		SSD1306_SETDISPLAYCLOCKDIV, //Display clock divide
		0x80,
		SSD1306_SETPRECHARGE, //precharge period
		0xF1,
		SSD1306_SETVCOMDETECT, 0x40,
		SSD1306_CHARGEPUMP, 0x14,
//		SSD1306_INVERTDISPLAY,//
		SSD1306_DISPLAYON };

static void _command(uint8_t command) {
	uint8_t commandBuffer[2];
	commandBuffer[0] = 0x80;
	commandBuffer[1] = command;

	OLEDi2c.dataBuffer = (uint8_t *) commandBuffer;
	OLEDi2c.dataSize = 2;
	i2c_sendData(&OLEDi2c);

	while (OLEDi2c.busBusy)
		;
}

static void _data(uint8_t data) {
	uint8_t dataBuffer[2];
	dataBuffer[0] = 0x40;
	dataBuffer[1] = data;

	OLEDi2c.dataBuffer = (uint8_t *) dataBuffer;
	OLEDi2c.dataSize = 2;
	i2c_sendData(&OLEDi2c);

	while (OLEDi2c.busBusy)
		;
}

static void _setColumn(uint8_t column) {

	if (column > 128)
		return;

	_command(SSD1306_SETLOWCOLUMN | (column & 0x0F));
	_command(SSD1306_SETHIGHCOLUMN | (column >> 4));
}

static void _setPage(uint8_t page) {
	if (page > 7)
		return;

	_command(0xB0 | page);
}

static void _setPos(uint8_t x, uint8_t y) {
	_setColumn(x);
	_setPage(y);
}

static void _setLine(uint8_t line) {
	if (line > 32)
		return;

	_command(SSD1306_SETSTARTLINE | line);
}

static void _writeChar(uint8_t c) {
	uint16_t c_point;

	c_point = c * 6;

	_data(font_6x7[c_point++]);
	_data(font_6x7[c_point++]);
	_data(font_6x7[c_point++]);
	_data(font_6x7[c_point++]);
	_data(font_6x7[c_point++]);
	_data(font_6x7[c_point]);
}

void ssd1306_init(void) {
	rcc_APB1ENR_set(RCC_APB1ENR_I2C1EN);
	rcc_AHB1ENR_set(RCC_AHB1ENR_GPIOBEN);

	gpio_pin_cfg(GPIOB, P8, gpio_mode_AF4_OD_FS);
	gpio_pin_cfg(GPIOB, P9, gpio_mode_AF4_OD_FS);

	OLEDi2c.I2Cx = I2C1;
	OLEDi2c.slaveAddress = SSD1306_ADDRESS;
	OLEDi2c.dataBuffer = (uint8_t *) initBuffer;
	OLEDi2c.dataSize = 28;

	i2c_init(&OLEDi2c);

	i2c_sendData(&OLEDi2c);

	while (OLEDi2c.busBusy)
		;
}

void ssd1306_clear(void) {
	uint16_t i, j;
	_setColumn(0);
	_setLine(0);

	for (j = 0; j < 8; j++) {
		_setPage(j);
		_setColumn(0);
		for (i = 0; i < 128; i++) {
			_data(0x00);
		}
	}
}

void ssd1306_displayBMP(uint8_t *bmp) {
	uint16_t i, j, k;
	_setColumn(0);
	_setLine(0);
	_setPage(0);
	k = 0;
	for (j = 0; j < 8; j++) {
		_setPage(j);
		_setColumn(0);
		for (i = 0; i < 128; i++) {
			_data(bmp[k++]);
		}
	}
}

void ssd1306_writeText(const char *text, uint8_t x, uint8_t y) {
	_setPos(x, y);
	while (*text) {
		_writeChar(*text);
		++text;
	}
}

void ssd1306_printf(uint8_t x, uint8_t y, char *format, ...) {
	va_list args;

	va_start(args, format);
	char buf[64];
	vsnprintf(buf, 64, format, args);

	ssd1306_writeText(buf, x, y);
}
